package com.SpringBootProject.Data;

import com.SpringBootProject.Entity.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
@Qualifier("dataSource2")
public class UserDaoImpl2 implements UserDao {

    private static Map<Integer,User> users;
    static{
        users = new HashMap<Integer,User>(){
            {
                put(1, new User(1,"Andre"));
                put(2, new User(2,"Ben"));
                put(3, new User(3,"Charlie"));
            }
        };
    }

    @Override
    public Collection<User> getAllUsers(){
        return this.users.values();
    }

    @Override
    public User getUserById(int id){
       return this.users.get(id);
    }

    @Override
    public void removeUserById(int id) {
        this.users.remove(id);
    }

    @Override
    public void updateUser(User user) {
        User u = users.get(user.getId());
        u.setName(user.getName());
        u.setResidence(user.getResidence());
        this.users.put(user.getId(),user);
    }

    @Override
    public void insertUser(User user) {
        this.users.put(user.getId(),user);
    }
}
