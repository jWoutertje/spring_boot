package com.SpringBootProject.Entity;

public class User {

    private int id;
    private String name;
    private String Residence;

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public User(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResidence() {
        return Residence;
    }

    public void setResidence(String residence) {
        Residence = residence;
    }
}
